from pattern.en import parsetree
from pattern.search import search
import math,time,re
from pymongo import MongoClient
import Levenshtein as lev


entity_names = []
normal_nouns = []
results = []
flag_entity = 0	
kinds_of_nouns = ['JJ','NN','NNP','NNP-PERS','NNS','VBN','VBG'] #TODO Be sure about NNP,VBG,NNS,VBN




def music_search(entry):
	results = {}
	client = MongoClient('127.0.0.1')
	db = client['entities']
	collection = db.music
	entry = re.split("[ ]+",entry)	
	qu = "_".join(entry).lower()
	cursor = collection.find({'$text': {'$search': qu}},{'score': {'$meta': 'textScore'}})
	cursor.sort([('score', {'$meta': 'textScore'})]).limit(1)
	if cursor.count() <= 0:
		pass
	elif cursor[0]['score'] >=1:
		checker = cursor[0]['name']
		if lev.ratio(str(checker.lower()),str(qu.lower())) == 1:
			for raw_line in cursor:
				genres = raw_line['genres']
				line = raw_line['name'].strip()
				line = re.sub("[_]+"," ",line)
				results = {"type":"music","name":line,"genres":genres}
	return results
	
def movie_search(entry):
	results = {}
	client = MongoClient('127.0.0.1')
	db = client['entities']
	collection = db.movies
	entry = re.split("[ ]+",entry)
	qu = "_".join(entry).lower()
	cursor = collection.find({'$text': {'$search': qu}},{'score': {'$meta': 'textScore'}})
	cursor.sort([('score', {'$meta': 'textScore'})]).limit(1)
	if cursor.count() <= 0:
		pass
	elif cursor[0]['score'] >=1:
		checker = cursor[0]['title']
		if lev.ratio(str(checker.lower().strip()),str(qu.lower().strip())) >= 0.9:
			for raw_line in cursor:
				genres = raw_line['genres']
				line = raw_line['title'].strip()
				line = re.sub("[_]+"," ",line)
				results = {"type":"movie","title":line,"genres":genres}
	return results


def extract_normal_nouns(tree):
	normal_nouns = []
	buff = []
	entry_types = []
	cc_flag = 0
	global count,kinds_of_nouns, brown_mapping1,entity_names
	for sentence in tree:
		for word in sentence:
			if word.string.lower() == 'music' or word.string.lower() == 'movie':
				pass
			elif word:
				if (word.tag in kinds_of_nouns) or (word.tag == 'CC') or (word.tag == 'DT'):
					if(word.tag == 'CC') and (cc_flag==0) and (buff != []):
						cc_flag = 1
						buff.append(word.string)
						entry_types.append(word.tag)
					elif (word.tag == 'DT'):
						dt_flag = 1
						buff.append(word.string)
						entry_types.append(word.tag)
					elif (word.tag in kinds_of_nouns):
						buff.append(word.string)
						entry_types.append(word.tag)
					else:
						cc_flag = 0
				else:
					if len(buff):
						for b in range(0,len(buff)):
							if buff[b].lower() == 'and' and b!=0:
								pass
							else:
								normal_nouns.append(buff[b:])
						buff = []
						entry_types = []
		if len(buff):
			for b in range(0,len(buff)):
				if buff[b].lower() == 'and' and b!=0:
					pass
				else:
					normal_nouns.append(buff[b:])

	return normal_nouns
	
def getResults(text):
	global normal_nouns,flag_entity,flag_noun,results
	results = []
	tagged_text = parsetree(text, relations = True, lemmata = True)  
	count=0
	candidates = extract_normal_nouns(tagged_text)
	for entry in candidates:
		entry = '_'.join(entry) ### Needs checking
		check = music_search(entry)
		if not check:
			check = movie_search(entry)
		if check:
			results.append(check)
	return results
